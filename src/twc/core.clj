(ns twc.core
  (:gen-class))

;; Triangle question

;; helper for valid triangle
(defn valid-triangle?
  "Checks triangle inequality theorem- sum of two sides must be greater than third side"
  [a b c]
  (and
      (> (+ a b) c)
      (> (+ b c) a)
      (> (+ a c) b)))

;; Isoceles is the only necessary helper function
;;   equilateral is an easy check with = function
;;   cond statement will resort of scalene with :else block
(defn isoceles?
  [a b c]
  (or (= a b)
      (= b c)
      (= a c)))

(defn triangle-type
  [a b c]
  (cond
      ;; valid check first, then equilateral
      ;;    equilateral also passes isoceles? test, so needs to come before
      (not (valid-triangle? a b c)) :invalid
      (= a b c) :equilateral
      (isoceles? a b c) :isoceles
      :else :scalene
      ))


;;;;; nth number of a list going backwards, not using

;; simple and straight forward.  Doesn't feel elegant though
(defn nth-from-last
  [n target]
  (if (> n (count target))
      nil
      (first (take-last n target))))

;; This is recursive, but feels wrong because I pass in the same value for n every call
;;added cond to catch exception of n larger than target
(defn another-nth-from-last
  [n target]
  (cond
      (= n (count target))(first target)
      (> n (count target)) nil
      :else (recur n (rest target))))


;;;



;; effective, but very simple with sets
;; uses a set as the first argument to every? function
(defn contains-all?
    "returns true if everything in the first collection is in the second collection"
    [list1 list2]
    (every? (set list2) list1))

;;attempt with recursion- work in progress
;
; (defn contains-all-recursive?
;     [list1 list2]
;     (if (first list1)))
