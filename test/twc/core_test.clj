(ns twc.core-test
  (:require [clojure.test :refer :all]
            [twc.core :refer :all]))

(deftest triangle-test
  (testing "Equilateral Triangle."
    (is (= :equilateral (triangle-type 5 5 5))))
  (testing "Isoceles Triangle"
    (is (= :isoceles (triangle-type 6 6 8))))
  (testing "Scalene Triangle"
    (is (= :scalene (triangle-type 6 7 8))))
  (testing "invalid triangle"
    (is (= :invalid (triangle-type 2 2 400)))))

(deftest nth-list
  (testing "another-nth-from-last test"
    (is (= 4 (another-nth-from-last 2 [1 2 3 4 5]))))
  (testing "nth-from-last test"
    (is (= 4 (nth-from-last 2 [1 2 3 4 5]))))
  (testing "another-nth-from-last out of bounds test"
    (is (= nil (another-nth-from-last 9 [1 2 3 4 5]))))
  (testing "nth-from-last out of bounds test"
    (is (= nil (nth-from-last 9 [1 2 3 4 5])))))

(deftest list-in-list-test
  (testing "list in list success"
    (is (= true (contains-all? [1 2 3 4] [ 11 1 22 2 33 3 4]))))
  (testing "returns false if all are not contained"
    (is (= false (contains-all? [1 2 3 4 3] [1 2 2 3 5])))))
